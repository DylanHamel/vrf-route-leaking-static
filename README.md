# VRF - Route Leaking

Dylan Hamel - <dylan.hamel@protonmail.com>

[https://www.cisco.com](https://www.cisco.com/c/en/us/support/docs/multiprotocol-label-switching-mpls/multiprotocol-label-switching-vpns-mpls-vpns/47807-routeleaking.html#diffvrfs)

## Schéma

![vrf_network.png](./schema/vrf_network.png)

## Introduction

De base, sans configuration l'output des commandes ```show ip route``` et ```show run | sec interface``` sont :

```
R1#show run | section interface
interface FastEthernet0/0
 no ip address
 shutdown
 duplex auto
 speed auto
interface FastEthernet0/1
 no ip address
 shutdown
 duplex auto
 speed auto
```

```
R1#show ip route
Codes: C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route

Gateway of last resort is not set
```



Si on configure deux adresses IPv4 sur les interfaces

* ```fa0/0``` = 192.168.0.1 /24
* ```fa0/1``` = 192.168.1.1 /24

Les outputs deviennent :

```
R1#show ip route
Codes: C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route

Gateway of last resort is not set

C    192.168.0.0/24 is directly connected, FastEthernet0/0
C    192.168.1.0/24 is directly connected, FastEthernet0/1
```

```
R1#show run | section interface
interface FastEthernet0/0
 ip address 192.168.0.1 255.255.255.0
 duplex auto
 speed auto
interface FastEthernet0/1
 ip address 192.168.1.1 255.255.255.0
 duplex auto
 speed auto
```



## Configuration simple de VRF 

Le but maintenant est de mettre le ```ClientA``` et le ```ClientB``` dans deux VRF différents.

#### Configuration ClientA

```
interface Loopback0
 ip address 1.1.1.1 255.255.255.0
!
interface FastEthernet0/0
 ip address 10.1.1.1 255.255.255.0
 duplex auto
 speed auto
 
ip route 0.0.0.0 0.0.0.0 10.1.1.254 name default-gateway
```

#### Configuration ClientB

```
interface Loopback0
 ip address 2.2.2.1 255.255.255.0
!
interface FastEthernet0/1
 ip address 10.2.2.1 255.255.255.0
 duplex auto
 speed auto

ip route 0.0.0.0 0.0.0.0 10.2.2.254 name default-gateway
```

#### Configuration Internet

On crée deux VRF, 1 par client

```
ip vrf CLIENTA

ip vrf CLIENTB
```

Configurer l'adresse IPv4 sur chaque interface et l'assigner dans le bon VRF

> Commencer par configurer le VRF !!

```
interface FastEthernet0/0
 description Link-ClientA
 ip vrf forwarding CLIENTA
 ip address 10.1.1.254 255.255.255.0
 duplex auto
 speed auto
!
interface FastEthernet0/1
 description Link-ClientB
 ip vrf forwarding CLIENTB
 ip address 10.2.2.254 255.255.255.0
 duplex auto
 speed auto
```

Si on regarde la table de routage par défaut du routeur, même les routes directement connectées ne sont pas affichées. Ces interfaces sont "dans" le VRF.

```
Internet#show ip route
Codes: C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route

Gateway of last resort is not set

------------------------------------------------------------------------------------------

Internet#show ip route vrf CLIENTA

Gateway of last resort is not set

     10.0.0.0/24 is subnetted, 1 subnets
C       10.1.1.0 is directly connected, FastEthernet0/0

------------------------------------------------------------------------------------------

Internet#show ip route vrf CLIENTB

Gateway of last resort is not set

     10.0.0.0/24 is subnetted, 1 subnets
C       10.2.2.0 is directly connected, FastEthernet0/1
```

On ne peut donc pas joindre les réseaux depuis la table de routage par défaut.

```
Internet#ping 10.1.1.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 10.1.1.1, timeout is 2 seconds:
.....
Success rate is 0 percent (0/5)
```

Avec des traces Wireshark, on se rend compte que les messages ICMP ne sortent logiquement sur aucun port.



### VRF - Leak

On peut donc ajouter des routes statiques dans les VRF qui seront intégrés à la table de routage global
Ajouter les routes dans les VRF.
Pour faire, cela, il faut ajouter ```global``` à la fin de la route.

```
ip route vrf CLIENTA 2.2.2.0 255.255.255.0 10.2.2.1 global
ip route vrf CLIENTB 1.1.1.0 255.255.255.0 10.1.1.1 global
```

à ce moment, la table de routage est encore vide. On aura seulement des paquets à destination de ```2.2.2.0/24``` et ```1.1.1.0 /24``` qui arrivent dans la table de routage "globale".

On peut donc ajouter des routes, dans la table de routage, qui vont définir sur quelle interface envoyer les paquets.

```
ip route 10.1.1.0 255.255.255.0 FastEthernet0/0
ip route 10.2.2.0 255.255.255.0 FastEthernet0/1
```

```
Internet#show ip route
Codes: C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route

Gateway of last resort is not set

     10.0.0.0/24 is subnetted, 2 subnets
S       10.2.2.0 is directly connected, FastEthernet0/1
S       10.1.1.0 is directly connected, FastEthernet0/0
```



Ci-dessous, un schéma qui représente les échanges entre les tables de routage.
En suivant les flèches, on comprend que le ```ClientA``` peut joindre ```ClientB``` avec l'IP source ```1.1.1.1```

```
ClientA#ping 2.2.2.1 source 1.1.1.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 2.2.2.1, timeout is 2 seconds:
Packet sent with a source address of 1.1.1.1
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 92/99/128 ms
```

![VRF-Leak.png](./schema/VRF-Leak.png)

```
ClientB#ping 1.1.1.1 source 2.2.2.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
Packet sent with a source address of 2.2.2.1
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 92/94/96 ms
ClientB#
```

